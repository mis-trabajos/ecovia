
from django.contrib import admin
from django.urls import path
from Aplicacion.views import *

urlpatterns = [
    path("admin/", admin.site.urls),
    path("",home,),
    path("home/",home,name="home"),
    path("registrarse/",registrar_usuario,name="registrarse"),
    path("iniciar_sesion/",iniciar_sesion,name="iniciar_sesion"),
    path("logout/", cerrar_sesion, name="cerrar_sesion"),
    path("crear_comentario/", crear_comentarios, name="crear_comentarios"),
    path('ver_comentarios/', ver_comentarios, name='ver_comentarios'),
    path('eliminar_comentario/<int:id>/', eliminar_comentario, name='eliminar_comentario'),
    path('editar_comentario/<int:id>/', editar_comentario, name='editar_comentario'),
    path('crear_respuesta/', crear_respuesta, name='crear_respuesta'),
   
]
