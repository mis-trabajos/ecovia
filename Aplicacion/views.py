from django.shortcuts import render,redirect, get_object_or_404
from django.contrib.auth.forms import  AuthenticationForm # para crear formularios
from django.contrib.auth import login,logout,authenticate
from django.db import IntegrityError
from .forms import *
from .models import *
from django.contrib.auth.decorators import login_required
# Create your views here.

def home(request):
    return render(request, "home.html")
from django.shortcuts import render, redirect
from .forms import RegistroForm

def registrar_usuario(request):
    form = RegistroForm()
    if request.method == 'POST':
        form = RegistroForm(request.POST)
        if form.is_valid():
            user = form.save()  # Guardar el nuevo usuario en la base de datos
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(request, username=username, password=password)
            login(request, user)  # Iniciar sesión automáticamente
            return redirect('home')  # Redirigir a la página de inicio después del registro exitoso
    return render(request, 'registro.html', {'form': form})

def iniciar_sesion(request):
    if request.method == "GET":
        return render(request, 'iniciar_sesion.html',{"form":AuthenticationForm})
    else:
        user = authenticate(request,username=request.POST["username"],password=request.POST["password"])
        if user is None: 
            return render(request, 'iniciar_sesion.html',{"form":AuthenticationForm,"error":"nombre de usuario o contraseña incorrecto"})    
        else:
            login(request,user)
            return redirect("home")
            
def cerrar_sesion(request):
    logout(request)
    return redirect("home")

def ver_comentarios(request):
    comentarios = Comentario.objects.all()
    respuestas = Respuesta.objects.all()

    if request.method == 'POST':
        respuesta_form = RespuestaForm(request.POST)
        if respuesta_form.is_valid():
            id = respuesta_form.cleaned_data['id']
            texto = respuesta_form.cleaned_data['texto']
            comentario = Comentario.objects.get(id=id)
            respuesta = Respuesta(comentario=comentario, usuario=request.user, texto=texto)
            respuesta.save()
            return redirect('ver_comentarios')
    else:
        respuesta_form = RespuestaForm()

    return render(request, 'ver_comentarios.html', {'comentarios': comentarios, 'respuestas': respuestas, 'respuesta_form': respuesta_form})

@login_required
def crear_respuesta(request):
    if request.method == 'POST':
        respuesta_form = RespuestaForm(request.POST)
        if respuesta_form.is_valid():
            comentario_id = respuesta_form.cleaned_data['comentario_id']
            texto = respuesta_form.cleaned_data['texto']
            comentario = Comentario.objects.get(id=comentario_id)
            respuesta = Respuesta(comentario=comentario, usuario=request.user, texto=texto)
            respuesta.save()
            return redirect('ver_comentarios')
    else:
        respuesta_form = RespuestaForm()
        
    return render(request, 'crear_respuesta.html', {'respuesta_form': respuesta_form})

@login_required
def eliminar_comentario(request, id):
  comentario = Comentario.objects.get(id=id)
  comentario.delete()
  return redirect('ver_comentarios')

@login_required
def editar_comentario(request,id):
    comentario = get_object_or_404(Comentario, id=id)
    
    if request.method == 'POST':
        form = ComentarioForm(request.POST, instance=comentario)
        if form.is_valid():
            form.save()
            return redirect('ver_comentarios')
    else:
        form = ComentarioForm(instance=comentario)
    
    return render(request, 'editar_comentario.html', {'form': form})
    
@login_required
def crear_comentarios(request):
    if request.method == 'GET':
        form = ComentarioForm()
        return render(request, 'crear_comentario.html', {'form': form})  
    else:
        form = ComentarioForm(request.POST)
        if form.is_valid():
            comentario = form.save(commit=False)
            comentario.usuario = request.user  # Asignar el usuario actual al comentario
            comentario.save()
            return redirect('ver_comentarios')
        else:
             print(form.errors)
    return render(request, 'crear_comentario.html', {'form': form})
