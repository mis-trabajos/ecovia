from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import *

class RegistroForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

class ComentarioForm(forms.ModelForm):
    class Meta:
        model = Comentario
        fields = ('texto',)
        
class RespuestaForm(forms.Form):
    comentario_id = forms.IntegerField(widget=forms.HiddenInput())
    texto = forms.CharField(widget=forms.Textarea(attrs={'rows': 4}))