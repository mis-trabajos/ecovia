
from django.db import models
from django.contrib.auth.models import User

class Comentario(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    texto = models.TextField(max_length=200)
    creacion = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return f'comentado por {self.usuario.username}'

class Respuesta(models.Model):
    comentario = models.ForeignKey(Comentario, on_delete=models.CASCADE)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    texto = models.TextField(max_length=200)
    fecha = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return f'Respuesta a "{self.comentario}" por {self.usuario.username}'