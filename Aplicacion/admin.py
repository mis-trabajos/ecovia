from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from .models import* 

admin.site.unregister(User)  # Desregistras el modelo User por defecto
admin.site.register(Comentario)

@admin.register(User)
class CustomUserAdmin(UserAdmin):
    # Personaliza las opciones de visualización y edición del modelo User
    list_display = ('username', 'email', 'is_staff', 'date_joined')
    list_filter = ('is_staff', 'is_superuser', 'groups')
    search_fields = ('username', 'email')
    ordering = ('-date_joined',)


